# Sobre

Estes documento README tem como objetivo fornecer as informações necessárias para realização do projeto de avaliação de candidatos.

# 🏗 O que fazer?

- Você deve realizar um fork deste repositório e, ao finalizar, enviar o link do seu repositório para a nossa equipe (ti@coins.com.br). Lembre-se, NÃO é necessário criar um Pull Request para isso, nós iremos avaliar e retornar por email o resultado do seu teste.

# 🚨 Requisitos

- A API deverá ser construída em **NodeJS** ou **NodeTS**
- Implementar autenticação e deverá seguir o padrão **JWT**.
- Caso seja desenvolvida em NodeJS o seu projeto terá que ser implementado em **ExpressJS** ou **Hapi**
- Para a comunicação com o banco de dados utilize algum **ORM**/**ODM**, como o **Sequelize**, por exemplo, porém não é necessário utilizar Migrations, mas é um diferencial.
- Bancos relacionais permitidos:
  - MySQL
  - Postgre
- Bancos não relacionais permitidos:
  - MongoDB
- Sua API deverá seguir os padrões Rest na construção das rotas e retornos
- Sua API deverá conter a collection/variáveis do **Postman** ou **Imnsomia** ou algum endpoint da documentação em openapi para a realização do teste
- É desejável que o teste esteja na liguagem  **JavaScript** buscando avaliar o entendimento completo da linguagem e não de estruturas ou dependências que abstraiam determinadas definições não alheias ao ECMAScript. No entanto, testes realizados em **TypeScript** também serão aceitos.
- Ao subir seu projeto, inclua um README que informe as etapas para deixar sua api funcionando. (Build, Deploy)

# 🕵🏻‍♂️ Itens a serem avaliados

- Estrutura do Projeto !Muito importante!
- Integrações de APIS externas
- Segurança da API, como autenticação, senhas salvas no banco, SQL Injection e outros
- Boas práticas da Linguagem/Framework
- Seu projeto deverá seguir o que foi exigido na seção [O que desenvolver?](##--o-que-desenvolver), porém testes 50% realizados podem chegar a ser avaliados.

# 🎁 Extra

Esses itens não são obrigatórios, porém desejados.

- Testes unitários
- Linter
- Code Formater

**Obs.: Lembrando que o uso de algum linter ou code formater irá depender da linguagem que sua API for criada**

# 🖥 O que desenvolver?

Você deverá criar uma API que o app vai consumir para que o usuário visualize as informações de sua conta e consiga realizar uma transação. Sua API deve conter as seguintes features:

- Usuário
  - Cadastro
    - Nome
    - Email
    - CEP (Integrar https://viacep.com.br/ para identificar os dados do endereço, e validar CEP)
    - Rua 
    - Número
    - Bairro
    - Cidade (Lógica simplista: O endpoint deve voltar erro se a cidade informada pelo usuário não for igual ao obtido no VIACEP)

  - Login
    - Email
    - Senha
  - Edição
  - Exclusão lógica (Desativação)

- Saldo

  - Ver Saldo
  - Ver Transações

- Recarga de Celular

  - Listagem (deverá ter filtro por Operadora de Celular ou Valor)
  - Detalhe do Serviço (Operadora, Como Usar, Valor)
  - Comprar (Pedindo senha)

# 🔗 Links

- Documentação JWT https://jwt.io/
- Via CEP: https://viacep.com.br/
